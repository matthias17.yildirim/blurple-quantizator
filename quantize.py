#!/usr/bin/env python3
from PIL import Image, ImageOps, ImageChops
from itertools import groupby
import difflib
import os
import sys

if len(sys.argv) != 5:
    print("Usage: python ./quantize.py <inputFile> <canvas> <x> <y>")
    exit()

palettedata = [ 244, 123, 103, ## START DEFAULT COLORS #####
                248, 165, 50,
                72, 183, 132,
                69, 221, 192,
                153, 170, 181,
                35, 39, 42,
                183, 194, 206,
                65, 135, 237,
                54, 57, 63,
                62, 112, 221,
                79, 93, 127,
                114, 137, 218,
                78,  93, 148,
                156, 132, 239,
                244, 127, 255,
                255, 255, 255, ##### END DEFAULT COLORS #####
                190, 77, 60,
                48, 16, 2,
                165, 107, 77,
                252, 150, 75,
                252, 119, 3,
                208, 179, 111,
                252, 194, 27,
                239, 219, 83,
                247, 238, 117,
                91, 144, 66,
                20, 51, 6,
                50, 205, 50,
                0, 255, 117,
                23, 161, 103,
                0, 153, 102,
                0, 214, 214,
                167, 186, 188, 
                72, 211, 248,
                104, 199, 231,
                53, 196, 247,
                2, 172, 239,
                0, 174, 255,
                32, 139, 210,
                0, 90, 166,
                8, 61, 119,
                18, 130, 253,
                66, 133, 244,
                47, 97, 232,
                88, 116, 232,
                218, 214, 248,
                130, 114, 176,
                208, 192, 229, 
                134, 39, 230,
                153, 51, 255,
                237, 177, 248,
                107, 51, 98,
                183, 0, 63,
                198, 59, 104,
                255, 183, 197,
                230, 45, 61,
                234, 35, 40,
                80, 80, 80,
                237, 52, 52,
                219, 35, 35,
                219, 35, 35,
                219, 35, 35,
                219, 35, 35,
                219, 35, 35,]
colors = {  '244, 123, 103': 'brll', ## START DEFAULT COLORS #####
            '248, 165, 50': 'hpsq',
            '72, 183, 132': 'bhnt',
            '69, 221, 192': 'blnc',
            '153, 170, 181': 'grpl',
            '35, 39, 42': 'nqbl',
            '183, 194, 206': 'ntgr',
            '65, 135, 237': 'ptnr',
            '54, 57, 63': 'dgry',
            '62, 112, 221': 'devl',
            '79, 93, 127': 'ntbl',
            '114, 137, 218': 'blpl',
            '78,  93, 148': 'dbpl',
            '156, 132, 239': 'brvy',
            '244, 127, 255': 'bstp',
            '255, 255, 255': 'whte', ##### END DEFAULT COLORS #####
            '190, 77, 60': 'flre',
            '48, 16, 2': 'zzba',
            '165, 107, 77': 'jlbr',
            '252, 150, 75': 'ntro',
            '252, 119, 3': 'nugg',
            '208, 179, 111': 'frnx',
            '252, 194, 27': 'blob',
            '239, 219, 83': 'swss',
            '247, 238, 117': 'aufr',
            '91, 144, 66': 'pepe',
            '20, 51, 6': 'farm',
            '50, 205, 50': 'ddlg',
            '0, 255, 117': 'tfkd',
            '23, 161, 103': 'ttsu',
            '0, 153, 102': 'swmp',
            '0, 214, 214': 'rswb',
            '167, 186, 188': 'wtch', 
            '72, 211, 248': 'miap',
            '104, 199, 231': 'mhxc',
            '53, 196, 247': 'stwr',
            '2, 172, 239': 'miit',
            '0, 174, 255': 'ccod',
            '32, 139, 210': 'kgsb',
            '0, 90, 166': 'iitr',
            '8, 61, 119': 'adys',
            '18, 130, 253': 'wocu',
            '66, 133, 244': 'goog',
            '47, 97, 232': 'tvlg',
            '88, 116, 232': 'nqnb',
            '218, 214, 248': 'angm',
            '130, 114, 176': 'nyte',
            '208, 192, 229': 'hrmy', 
            '134, 39, 230': 'pldl',
            '153, 51, 255': 'brdl',
            '237, 177, 248': 'pure',
            '107, 51, 98': 'foof',
            '183, 0, 63': 'scps',
            '198, 59, 104': 'ayna',
            '255, 183, 197': 'swlp',
            '230, 45, 61': 'dsts',
            '234, 35, 40': 'mrvl',
            '80, 80, 80': 'dklz',
            '237, 52, 52': 'lcas',
            '219, 35, 35': 'bllk',}

palimage = Image.new('P', (16, 16))
palimage.putpalette(palettedata * 4)
input_image = Image.open(sys.argv[1]) # input file
print(input_image.size)
#m_size = int(sys.argv[3]) # max size
w = input_image.size[0]
h = input_image.size[1]
print(f"Input img: {w}x{h}")
#if sys.argv[2] == 'w':
    #ratio = m_size / w
    #w = m_size
    #h = int(h * float(ratio))
#else:
    #ratio = m_size / h
    #w = int(w * float(ratio))
    #h = m_size
print(f"Output img: {w}x{h}")
input_image = input_image.resize((w, h), 4)  # resampling filter Image.NEAREST (0), Image.LANCZOS (1), Image.BILINEAR (2), Image.BICUBIC (3), Image.BOX (4) or Image.HAMMING (5)

mask_image = Image.new("1", input_image.size, 1)
if input_image.mode == "RGBA":
    mask_pixels = mask_image.load()
    alpha_channel = input_image.load()
    for y in range(input_image.size[1]):
        for x in range(input_image.size[0]):
            if alpha_channel[x, y][3] == 0:
                mask_pixels[x, y] = 0
            else:
                mask_pixels[x, y] = 1
    input_image = input_image.convert("RGB")

new_image = input_image.quantize(colors=17, method=3, kmeans=0, palette=palimage, dither=0) # method – 0 = median cut 1 = maximum coverage 2 = fast octree 3 = libimagequant

canvas = Image.open(sys.argv[2])
canvas = canvas.resize((425, 425), 0)
canvas.paste(new_image, (int(sys.argv[3])+24, int(sys.argv[4])+24), mask_image)

st_img = Image.open(sys.argv[2])
st_img = st_img.resize((425, 425), 0)

diff = Image.new("RGBA", canvas.size)


if not os.path.exists(sys.argv[1].split('.')[0]):
    os.makedirs(sys.argv[1].split('.')[0])

map = Image.new("RGBA", canvas.size)
map.paste(new_image, (int(sys.argv[3])+24, int(sys.argv[4])+24), mask_image)
map = map.load()

count = 0
N = []
TXT = []
with open(f"{sys.argv[1].split('.')[0]}/Commands_{sys.argv[1].split('.')[0]}.txt", 'w') as f:
    for i in range(0, 400):
        for j in range(0, 400):
            if map[j, i][3] == 255:
                if ', '.join(difflib.get_close_matches(str(st_img.getpixel((j, i))[:3])[1:-1], colors, 1)) != str(map[j, i][:3])[1:-1]:
                    TXT.append(f"p/place {j - 24} {i - 24} {colors[str(map[j, i][:3])[1:-1]]}")
                    N.append(colors[str(map[j, i][:3])[1:-1]])
                    count += 1
                    diff.putpixel((j, i), map[j, i])  
    N = [list(j) for i, j in groupby(sorted(N))]
    col = []
    for x in N:
        col.append(x[0])
    f.write(f"#===== STATS =====#\nRemaining pixels: {count}\nOrigin point: {sys.argv[3]} {sys.argv[4]}\nw : {input_image.width} | h : {input_image.height}\nColors used:\n"+'\n'.join(col)+"\n#=================#\n\n"+'\n'.join(TXT))

diff.save(f"{sys.argv[1].split('.')[0]}/Difference_{sys.argv[1].split('.')[0]}.png")
print("==============================")
print("Colors used :")
print('\n'.join(col))
print("=====")
print(f"Pixels to place : {count}")
print("=====")
print(f"Commands saved in {sys.argv[1].split('.')[0]}/Commands_{sys.argv[1].split('.')[0]}.txt")
canvas.save(f"{sys.argv[1].split('.')[0]}/Preview_{sys.argv[1].split('.')[0]}.png") #.save/show
print(f"Preview saved in {sys.argv[1].split('.')[0]}/Preview_{sys.argv[1].split('.')[0]}.png")

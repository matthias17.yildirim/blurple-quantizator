**Features:** 
- Converts input file to specified (hardcoded) palette
- Places on canvas on **x**, **y**
- Generates a list of required commands to paint
- Pixels already placed check
- "difference image making"

**Usage:**
1 - Place an image you want to place on the canvas in the same directory **__at the format 1:1 pixel__** (1 pixel on the image = 1 pixel on the canvas)
2 - Place the canvas image in the same directory
3 - do `python ./quantize.py \<inputFile\> \<canvas\> \<x\> \<y\>`
4 - Admire the result

##### Do not use relative paths, all files must be placed in a single directory.

**Example:** python ./quantize.py image.png board_1-1.png 0 0
